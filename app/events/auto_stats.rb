class AutoStats
  # msg => parameters
  kvsubscribe_stat_valid_symbols = {
      :real_income_production_date_rk_money_msg => %I[production, money],
      :real_income_production_buy_order_customer_date_rk_money_msg => %I[:customer, :money],
      :material_rk_money_msg => %I[:customer, :money],
  }
  # msg => models of stats
  kvsubscribe_stat_model = {
      :material_rk_money_msg => [MaterialLog],
  }

  # model name => class
  model_name_class = {
      :MaterialLog => MaterialLog,
  }

  kvsubscribe_stat_valid_symbols.each do |msg, params|
    EventBus.subscribe(msg) do |payload|
      model_name_arr = kvsubscribe_stat_model[msg]
      model_name_arr.each do |stat_class|
        new_log = stat_class.new
        obj = payload[:current]
        used_money = obj[:price] * obj[:price]
        # Lambda用于填统计log对象的属性，这里是一个mock
        new_log = obj.material_logs.new('detail' => 'detail', 'cost' => used_money)
        # params.each do |param|
        #   new_log.write_attribute(param, payload[param])
        # end
        new_log.commit  # log created
      end
    end
  end

end