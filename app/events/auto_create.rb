class AutoCreate
  #auto create
  auto_create_models = [:Material]

  material_block = lambda {|form, user| form.estimate_delivery_date + form.timespan.day}

  auto_create_models.each do |modelName|
    EventBus.subscribe("#{modelName}_commit_successful") do |payload|
      puts payload.inspect
    end
  end
end