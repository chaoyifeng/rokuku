class ModelListener
  @model_names = [:Product] # class name of models that related to log

  # def initialize(&block)
  #   instance_eval(&block)
  # end

  @model_names.each do |name|
    # successful_method_name="#{name.to_sym}_create_successful"
    # failure_method_name="#{name.to_sym}_create_failed"
    successful_method_name="#{name.to_sym}_create_successful".to_s
    failure_method_name="#{name.to_sym}_create_failed".to_s
    define_method successful_method_name do |object|
      puts 'successful_method_name'
      puts object.inspect
    end
    define_method failure_method_name do |object|
      puts 'failure_method_name'
      puts object.inspect
    end
  end
end