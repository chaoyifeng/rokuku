require 'singleton'

# cache related, would call these methods when model's create/update is triggered
class CacheListeners
  include Singleton

  def Product_create_successful(args)
    puts 'Product_create_successful'
    Material.create(:name => 'FromMyListenersInMaterial', :count => 233, :price => 321)
    ProductCache.create(:productID => args[:id], :productName => args[:name])
  end

  def Product_create_failed(args)
    puts 'Product_create_failed'
  end

  def Product_update_successful(args)
    puts 'Product update successful'
    @productCache = ProductCache.where(:productID => args[:id])
    @productCache.update(:productID => args[:id], :productName => args[:name])
  end
end
