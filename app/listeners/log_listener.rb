class LogListener
  @model_names = [:MaterialLog] # class name of models that related to log

  @model_names.each do |name|
    successful_method_name="#{name.to_sym}_create_successful"
    failure_method_name="#{name.to_sym}_create_failed"
    define_method(successful_method_name) do
      puts log.inspect
    end
    define_method(failure_method_name) do
      puts log.inspect
    end
  end
end