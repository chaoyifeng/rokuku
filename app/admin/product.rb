ActiveAdmin.register Product do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
    permitted = [:name, :count, :materials, {
        materials_attributes: [
            :id, :name, :count
        ]}]
    permitted
  end

  before_create do |product|
    # product.creator = current_user
  end

  # controller do
  #   def create
  #     # @product = create
  #     # @product.on(:product_creation_successful) { |product| redirect_to products_path }
  #     # @product.on(:product_creation_failed)     { |product| render action: 'new'  }
  #     # @product.commit
  #   end
  # end


  form do |f|
    f.inputs do
      f.input :name
      f.input :count
    end

    #TODO: Can't add material relationship in new product?
    # f.inputs do
    #   f.has_many :materials, allow_destroy: true do |i|
    #     i.inputs :material
    #   end
    # end
    f.actions
  end

end
