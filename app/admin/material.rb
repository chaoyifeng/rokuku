require_relative '../listeners/log_listener'
require_relative '../listeners/cache_listener'

# include Wisper::Publisher

ActiveAdmin.register Material do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  log_name = :MaterialLog # class name of models that related to log
  log_successful_method_name="#{log_name}_create_successful"
  log_failure_method_name="#{log_name}_create_failed"

  permit_params do
    permitted = [:name, :price, :count]
    # permitted << :other if params[:action] == 'create' && current_user.admin?
    permitted
  end

  member_action :purchase, method: :get do
    @material = Material.find(params[:id])
    @cost = @material.price * @material.count
    @material.status = 1
    @material.commit
    # redirect_to collection_path, notice: 'Purchased!'
  end

  member_action :withdraw, method: :get do
    @material = Material.find(params[:id])
    @cost = -@material.price * @material.count
    @material.status = 0
    @material.commit
  end

  actions :all, except: [:update, :destroy]

  index do
    column :name
    column :price
    column :count
    actions defaults: true do |material|
      item 'Show', resource_path(material)
      text_node " "
      # item 'Submit an edit request', new_admin_edit_request_path(:edit_request => {:material_id => material.id})
      # item 'Submit an edit request', submit_edit_material_path(material)
      text_node " "
      case Material.statuses[material[:status]]
        when 0
          item 'Purchase', purchase_admin_material_path(material)
        when 1
          item 'Withdraw', withdraw_admin_material_path(material)
        else
          # type code here
      end
      # item 'Submit an edit request', submit_edit_material_path(material)
      text_node " "
    end
  end
end
