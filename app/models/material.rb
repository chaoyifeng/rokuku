class Material < ApplicationRecord
  include Commit
  include SendStat

  has_many :material_logs

  enum status: { available: 0, purchased: 1 ,unavailable: 2 }

  after_create do
    send_stat
  end
end
