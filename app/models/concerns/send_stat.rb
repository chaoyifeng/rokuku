module SendStat
  # 需要统计的模型 -> 对应的统计消息
  @@model_msgs = {
      :Material => :material_rk_money_msg
  }

  def send_stat
    changed_hash = self.changes
    classname = self.class.name.to_sym
    if @@model_msgs.key?(classname)
      EventBus.announce(@@model_msgs[classname], current: self, changed: changed_hash)
    end
  end
end


