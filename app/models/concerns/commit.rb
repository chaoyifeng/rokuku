module Commit
  def commit(_attrs = nil)
    assign_attributes(_attrs) if _attrs.present?
    changed_hash = self.changes
    result = save
    classname = self.class.name
    if result
      EventBus.announce("#{classname}_commit_successful", current: self, changed: changed_hash)
    else
      EventBus.announce("#{classname}_commit_failed", current: self, changed: changed_hash)
    end
    result
  end
end


