class Product < ApplicationRecord
  has_many :materials
  accepts_nested_attributes_for :materials, :allow_destroy => true

  # after_create     :product_create_successful
  # after_update     :product_update_successful
  # after_validation :product_create_failed, on: :create

  private

  # def product_create_successful
  #   broadcast(:Product_create_successful, self)
  # end
  #
  # def product_update_successful
  #   broadcast(:Product_update_successful, self)
  # end
  #
  # def product_create_failed
  #   broadcast(:Product_create_failed, self) if errors.any?
  # end
end
