class AddStatusEnumToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :status, :integer, default: 0
  end
end
