class AddLogNameToMaterialLogs < ActiveRecord::Migration[5.1]
  def change
    add_column :material_logs, :detail, :string
  end
end
