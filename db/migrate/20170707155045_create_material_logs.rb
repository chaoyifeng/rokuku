class CreateMaterialLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :material_logs do |t|
      t.float :cost
      t.references :material, foreign_key: true

      t.timestamps
    end
  end
end
