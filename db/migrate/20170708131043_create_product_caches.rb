class CreateProductCaches < ActiveRecord::Migration[5.1]
  def change
    create_table :product_caches do |t|
      t.string :productName
      t.integer :productID

      t.timestamps
    end
  end
end
